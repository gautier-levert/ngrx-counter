import {createSelector} from '@ngrx/store';
import {counterFeatureKey, CounterState} from './counter.reducer';

export const selectCounterFeature = (state: any) => state[counterFeatureKey];

export const count = createSelector(
  selectCounterFeature,
  (state: CounterState) => state.count
);
